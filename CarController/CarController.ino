/* Car direction variables */
#include <Servo.h> 

// Bin variables
#define echo_pin 11 // Echo Pin
#define trig_pin 12 // Trigger Pin
#define FORWARD_PWM_PIN 8    // IN1 from H-Bridge connected to digital pin 9
#define BACKWARD_PWM_PIN  9    // IN2 from H-Bridge connected to digital pin 1

//Servo variables 
#define servo_pin 10         // Servo connected to digital pin 11
Servo myservo;  // create servo object to control a servo 

//Car static variables
int STEER_MAX_ANGLE = 18;
int STEER_ANGLE = 18;
int STEER_OFFSET = 93; //Offset: 93 degrees
int VEL = 20;
int MAX_VEL = 98;
int INVALID_STEER = -1000;

//Possible Driver commands
const char CMD_01_ACCELERATE = 1;
const char CMD_02_DECELERATE = 2;
const char CMD_03_STOP_ACCELERATE = 3;
const char CMD_04_STOP_DECELERATE = 4;
const char CMD_05_TURN_RIGHT = 5;
const char CMD_06_TURN_LEFT = 6;
const char CMD_07_STOP_RIGHT = 7;
const char CMD_08_STOP_LEFT = 8;
const char CMD_09_STOP_CAR = 9;

bool stop_car;
int movement_state;
int steer_state;
char buffer_clear[8];
char cmd;

// Functionality
void setup() {
  Serial.begin(9600);
  //Serial1.begin(9600);

  myservo.attach(servo_pin);  // attaches the servo pin to the servo object 
  pinMode(trig_pin, OUTPUT);
  pinMode(echo_pin, INPUT);
}

void loop() {
  if(Serial.available()){
    cmd = Serial.read();
    Serial.readBytes((char *)buffer_clear,Serial.available()); //Clear read buffer

    if(cmd == CMD_09_STOP_CAR) {
      stop_car = true;
    }
    if(!stop_car)
      update_car_state(cmd);
    //Serial1.println(cmd);
  }
}

/* Update car state with new driver command */

void update_car_state(char cmd){
  switch(cmd){
    case CMD_01_ACCELERATE:
        accelerate(VEL);
        break;
    case CMD_02_DECELERATE:
        decelerate();
        break;
    case CMD_03_STOP_ACCELERATE:
        disable_movement(FORWARD_PWM_PIN);
        movement_state = CMD_03_STOP_ACCELERATE;
        break;
    case CMD_04_STOP_DECELERATE:
        movement_state = CMD_04_STOP_DECELERATE;
        break;
    case CMD_05_TURN_RIGHT:
        steer(CMD_05_TURN_RIGHT);
        break;
    case CMD_06_TURN_LEFT:
        steer(CMD_06_TURN_LEFT);
        break;
    case CMD_07_STOP_RIGHT:
        steer(CMD_07_STOP_RIGHT);
        break;
    case CMD_08_STOP_LEFT:
        steer(CMD_08_STOP_LEFT);
        break;
  }
}

/* Main functions */

 void accelerate(unsigned int vel) {
    if(movement_state == CMD_01_ACCELERATE) // Already in acceleration mode
      return;

    if(vel <= 0) {
      Serial.print("[ERROR] Forward velocity must be higher than 0 but [VAL] given.");
      return;
    }

    //Protect from reverse rotation
    disable_movement(BACKWARD_PWM_PIN);
    
    //Max velocity restriction
    if(vel > MAX_VEL)
      vel = MAX_VEL;
      
    //Convert value to another range
    vel = vel * 255 / 100;
    //Write velocity to pin
    analogWrite(FORWARD_PWM_PIN, vel);

    movement_state = CMD_01_ACCELERATE;
 }

 void decelerate() {
    if(movement_state == CMD_02_DECELERATE) // Already in acceleration mode
      return;

    //Protect from reverse rotation
    disable_movement(FORWARD_PWM_PIN);
    disable_movement(BACKWARD_PWM_PIN);

    movement_state = CMD_02_DECELERATE;
 }

 void steer(char cmd) {
    if(cmd == steer_state)
      return;

    int steer_angle;
    steer_angle = get_steer_angle_from_cmd(cmd);
    if(steer_angle != INVALID_STEER) {
      myservo.write(steer_angle + STEER_OFFSET);
      steer_state = cmd;     
    }
 }

 /* Auxiliary functions */

 void disable_movement(int bin) {
    analogWrite(bin, 0);
    delay(30);
 }

 int get_steer_angle_from_cmd(char cmd) {
    switch(cmd){
      case CMD_05_TURN_RIGHT:
          return -STEER_ANGLE;
      case CMD_06_TURN_LEFT:
          return STEER_ANGLE;
      case CMD_07_STOP_RIGHT:
          return 0;
      case CMD_08_STOP_LEFT:
          return 0;
    }
    return INVALID_STEER;
}
